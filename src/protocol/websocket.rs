use std::convert::{
    From,
    Into
};
use byteorder::{
    ByteOrder,
    BigEndian
};

#[derive(Debug)]
pub struct WebSocketFrame {
    pub fin: bool,
    pub opcode: OpCode,
    pub is_masked: bool,
    pub payload_len: u64,
    pub mask: [u8; 4],
    pub payload: Vec<u8>,
}

#[repr(u8)]
#[derive(Debug)]
pub enum OpCode {
    Continuation,
    Text,
    Binary,
    Ping,
    Pong,
    Reserved,
    ConnectionClose,
    Invalid,
}

impl From<u8> for OpCode {
    fn from(i: u8) -> Self {
        match i {
            0 => OpCode::Continuation,
            1 => OpCode::Text,
            2 => OpCode::Binary,
            3..=7 => OpCode::Reserved,
            8 => OpCode::ConnectionClose,
            9 => OpCode::Ping,
            10 => OpCode::Pong,
            11..=15 => OpCode::Reserved,
            _ => OpCode::Invalid,
        }
    }
}

impl Into<Vec<u8>> for WebSocketFrame {
    fn into(self) -> Vec<u8> {
        let mut result = Vec::new();

        result.push(0b1000_0000 | self.opcode as u8);

        match self.payload_len {
            0..=125 => {
                result.push(0b0000_0000 | self.payload_len as u8);
            },
            126..=65536 => {
                result.push(0b0000_0000 | 126);
                let num : [u16; 1] = [self.payload_len as u16];
                let mut buf : [u8; 2] = [0;2];
                BigEndian::write_u16_into(&num, &mut buf);
                for byte in buf.iter() {
                    result.push(*byte);
                }
            },
            65537..=std::u64::MAX => {
                result.push(0b0000_0000 | 127);
                let num : [u64; 1] = [self.payload_len as u64];
                let mut buf : [u8; 4] = [0;4];
                BigEndian::write_u64_into(&num, &mut buf);
                for byte in buf.iter() {
                    result.push(*byte);
                }
            }
        };

        for byte in self.payload.iter() {
           result.push(*byte); 
        }

        result
    }
}

impl WebSocketFrame {
    pub fn decode_payload(&mut self) {
        for i in 0..self.payload_len {
            self.payload[i as usize] = &self.payload[i as usize] ^ self.mask[i as usize % 4];
        }
    }

    pub fn text_response(text : &str) -> Self {
        let payload = text.as_bytes().to_owned();
        WebSocketFrame {
            fin : true,
            is_masked : false,
            mask : [0;4],
            opcode : OpCode::Text,
            payload_len : payload.len() as u64,
            payload
        }
    }
}