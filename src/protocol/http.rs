use base64;
use sha1::{Digest, Sha1};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug)]
pub struct HttpRequest {
    pub method: HttpMethod,
    pub path: String,
    pub version: String,
    pub headers: HashMap<String, String>,
}

pub struct HttpResponse {
    pub version: String,
    pub status_code: ResponseStatus,
    pub headers: HashMap<String, String>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum HttpMethod {
    GET,
    Other(String),
}

pub enum ResponseStatus {
    SwitchingProtocols,
}

impl fmt::Display for ResponseStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ResponseStatus::SwitchingProtocols => write!(f, "101 Switching Protocols"),
        }
    }
}

impl fmt::Display for HttpResponse {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "HTTP/{} {}\r\n", self.version, self.status_code)?;
        for (key, value) in self.headers.iter() {
            write!(f, "{}: {}\r\n", key, value)?;
        }
        write!(f, "{}", "\r\n")
    }
}

const WEBSOCKETMAGICSTR: &str = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

pub fn is_websocket_upgrade_request(request: &HttpRequest) -> bool {
    let is_get = request.method == HttpMethod::GET;
    let is_11 = request.version == "1.1";
    let is_websocket_upgrade = request
        .headers
        .get("Upgrade")
        .map(|h| h == "websocket")
        .unwrap_or(false);
    let is_conn_upgrade = request
        .headers
        .get("Connection")
        .map(|h| h.contains("Upgrade"))
        .unwrap_or(false);
    let has_key = request.headers.get("Sec-WebSocket-Key").is_some();

    is_get && is_11 && is_websocket_upgrade && is_conn_upgrade && has_key
}

pub fn prepare_websocket_upgrade_response(request: &HttpRequest) -> HttpResponse {
    let version = "1.1";
    let status = ResponseStatus::SwitchingProtocols;
    let accept = request
        .headers
        .get("Sec-WebSocket-Key")
        .map(|h| encode_key(h))
        .expect("Failed to hash key");

    let mut headers: HashMap<String, String> = HashMap::new();
    headers.insert("Connection".into(), "Upgrade".into());
    headers.insert("Upgrade".into(), "websocket".into());
    headers.insert("Sec-WebSocket-Accept".into(), accept);

    HttpResponse {
        version: version.into(),
        status_code: status,
        headers,
    }
}

fn encode_key(key: &str) -> String {
    let acc_str = format!("{}{}", key, WEBSOCKETMAGICSTR);

    let mut sha = Sha1::new();
    sha.input(acc_str);

    let hash = sha.result();

    base64::encode(&hash[..])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode_key() {
        let key = "dGhlIHNhbXBsZSBub25jZQ==";

        let accept = encode_key(key);

        assert_eq!("s3pPLMBiTxaQ9kYGzzhZRbK+xOo=", accept);
    }
}
