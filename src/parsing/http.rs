use crate::protocol::http;
use nom::{
    bytes::complete::{is_not, tag, take_while},
    character::{
        complete::{crlf, digit1, multispace0, space1},
        is_alphabetic, is_alphanumeric,
    },
    combinator::{map, map_res, recognize},
    multi::many0,
    sequence::{terminated, tuple},
    IResult,
};
use nom_trace::tr;
use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::str;

lazy_static! {
    static ref TCHARS: HashSet<u8> = {
        let mut m = HashSet::new();
        m.insert(b'!');
        m.insert(b'#');
        m.insert(b'$');
        m.insert(b'%');
        m.insert(b'&');
        m.insert(b'*');
        m.insert(b'+');
        m.insert(b'-');
        m.insert(b'.');
        m.insert(b'^');
        m.insert(b'_');
        m.insert(b'`');
        m.insert(b'|');
        m.insert(b'~');
        m
    };
}

impl http::HttpMethod {
    fn from_bytes(bytes: &[u8]) -> Result<http::HttpMethod, String> {
        match str::from_utf8(bytes) {
            Ok("GET") => Ok(http::HttpMethod::GET),
            Ok(other) => Ok(http::HttpMethod::Other(other.into())),
            Err(e) => Err(e.to_string()),
        }
    }
}

fn method(input: &[u8]) -> IResult<&[u8], http::HttpMethod> {
    trace(
        "method",
        map_res(take_while(is_alphabetic), http::HttpMethod::from_bytes),
    )(input)
}

fn request_target(input: &[u8]) -> IResult<&[u8], &str> {
    trace(
        "target",
        map_res(
            take_while(|ch| is_alphanumeric(ch) || ch == b'/'),
            str::from_utf8,
        ),
    )(input)
}

fn http_tag(input: &[u8]) -> IResult<&[u8], &[u8]> {
    trace("http_tag", tag("HTTP/"))(input)
}

fn http_version(input: &[u8]) -> IResult<&[u8], &str> {
    trace(
        "version",
        map_res(recognize(tuple((digit1, tag("."), digit1))), str::from_utf8),
    )(input)
}

fn http_request_line(input: &[u8]) -> IResult<&[u8], http::HttpRequest> {
    trace(
        "request_line",
        map(
            tuple((
                method,
                space1,
                request_target,
                space1,
                http_tag,
                http_version,
                tag("\r\n"),
            )),
            |(method, _, path, _, _, version, _)| http::HttpRequest {
                method,
                path: path.into(),
                version: version.into(),
                headers: HashMap::new(),
            },
        ),
    )(input)
}

fn tchar(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(
        take_while(|ch| is_alphanumeric(ch) || TCHARS.contains(&ch)),
        str::from_utf8,
    )(input)
}

fn vchar(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(is_not("\r\n\t"), str::from_utf8)(input)
}

fn http_header(input: &[u8]) -> IResult<&[u8], (&str, &str)> {
    trace(
        "http_header",
        map(
            tuple((tchar, tag(":"), multispace0, vchar, crlf)),
            |(key, _, _, value, _)| (key, value),
        ),
    )(input)
}

fn http_headers(input: &[u8]) -> IResult<&[u8], Vec<(&str, &str)>> {
    terminated(many0(http_header), crlf)(input)
}

fn http_request_impl(input: &[u8]) -> IResult<&[u8], (http::HttpRequest, Vec<(&str, &str)>)> {
    let r = tr("default", "main", tuple((http_request_line, http_headers)))(input);

    print_trace!();

    r
}

pub fn http_request(input: &[u8]) -> Result<http::HttpRequest, String> {
    match http_request_impl(input) {
        Ok((_, (mut request, headers))) => {
            for (key, value) in headers.iter() {
                request
                    .headers
                    .insert((*key).to_string(), (*value).to_string());
            }
            Ok(request)
        }
        Err(e) => Err(e.to_string()),
    }
}

fn trace<I, O, E, F>(name: &'static str, f: F) -> impl Fn(I) -> IResult<I, O, E>
where
    nom_trace::Input: From<I>,
    F: Fn(I) -> IResult<I, O, E>,
    I: Clone,
    O: Debug,
    E: Debug,
{
    tr("default", name, f)
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_parse_http_method_ok() {
        let methoda = b"GET /chat HTTP/1.1";

        let result = method(methoda);

        assert_eq!(Ok((&b" /chat HTTP/1.1"[..], http::HttpMethod::GET)), result);
    }

    #[test]
    fn test_parse_http_method_wrong() {
        let methoda = b"POST";

        let result = method(methoda);

        assert_eq!(
            Ok((&[][..], http::HttpMethod::Other("POST".into()))),
            result
        );
    }

    #[test]
    fn test_parse_request_path() {
        let path = b"/chat HTTP/1.1";

        let result = request_target(path);

        let rem = &b" HTTP/1.1"[..];
        assert_eq!(Ok((rem, "/chat")), result);
    }

    #[test]
    fn test_http_tag_parse() {
        let input = b"HTTP/1.1";

        let result = http_tag(input);

        assert_eq!(Ok((&b"1.1"[..], &b"HTTP/"[..])), result)
    }

    #[test]
    fn test_http_version_parse() {
        let input = b"1.1\r";

        let result = http_version(input);

        assert_eq!(Ok((&b"\r"[..], "1.1")), result)
    }

    #[test]
    fn test_first_line_parse() {
        let cases: Vec<&[u8]> = vec![b"GET /chat HTTP/1.1\r\n"];

        for line in cases.iter() {
            let request = http_request_line(line);

            let request = request.unwrap().1;

            assert_eq!(&http::HttpMethod::GET, &request.method);
            assert_eq!("/chat", &request.path);
            assert_eq!("1.1", &request.version);
        }
    }

    #[test]
    fn test_http_header() {
        let cases: Vec<&[u8]> = vec![b"Upgrade: websockets\r\n"];

        for case in cases.iter() {
            let header = http_header(case);
            let header = header.unwrap().1;
            assert_eq!(("Upgrade", "websockets"), header);
        }
    }

    #[test]
    fn test_http_headers() {
        let cases: Vec<&[u8]> = vec![b"Upgrade: websockets\r\nConnection: upgrade\r\n\r\n"];

        for case in cases.iter() {
            let header = http_headers(case);
            let header = header.unwrap().1;
            assert_eq!(
                vec![("Upgrade", "websockets"), ("Connection", "upgrade")],
                header
            );
        }
    }

    #[test]
    fn test_http_request() {
        let cases: Vec<&[u8]> =
            vec![b"GET /chat HTTP/1.1\r\nUpgrade: websockets\r\nConnection:upgrade\r\n\r\n"];

        for case in cases.iter() {
            let request = http_request(case);
            let request = request.unwrap();

            assert_eq!(http::HttpMethod::GET, request.method);
            assert_eq!("/chat", request.path);
            assert_eq!("1.1", request.version);
            assert_eq!(
                Some(&"websockets".to_owned()),
                request.headers.get("Upgrade")
            );
            assert_eq!(
                Some(&"upgrade".to_owned()),
                request.headers.get("Connection")
            );
        }
    }
}
