use crate::{error, protocol::websocket};
use nom::{
    bits::{
        bits,
        streaming::{tag, take},
    },
    bytes,
    combinator::map,
    sequence::tuple,
    IResult,
};
use std::iter::FromIterator;

fn fin_and_opcode(input: &[u8]) -> IResult<&[u8], (bool, websocket::OpCode)> {
    map(
        bits(tuple((
            take::<_, _, _, (_, _)>(1usize),
            tag::<_, _, _, (_, _)>(0, 3usize),
            take::<_, _, _, (_, _)>(4usize),
        ))),
        |(fin, _, opcode): (u8, u8, u8)| (fin != 0, opcode.into()),
    )(input)
}

fn is_mask_and_payload_length(input: &[u8]) -> IResult<&[u8], (bool, u64)> {
    map(
        bits(tuple((
            take::<_, _, _, (_, _)>(1usize),
            take::<_, _, _, (_, _)>(7usize),
        ))),
        |(is_mask, init_payload): (u8, u64)| (is_mask != 0, init_payload),
    )(input)
}

fn extended_payload_length16(input: &[u8]) -> IResult<&[u8], u64> {
    bits(take::<_, _, _, (_, _)>(16usize))(input)
}

fn extended_payload_length64(input: &[u8]) -> IResult<&[u8], u64> {
    map(
        bits(tuple((
            tag::<_, _, _, (_, _)>(0, 1usize),
            take::<_, _, _, (_, _)>(7usize),
        ))),
        |(_, payload_length): (u8, u64)| payload_length,
    )(input)
}

fn masking_key(input: &[u8]) -> IResult<&[u8], [u8; 4]> {
    bits(
        map(
            tuple((
                take::<_, _, _, (_, _)>(8usize),
                take::<_, _, _, (_, _)>(8usize),
                take::<_, _, _, (_, _)>(8usize),
                take::<_, _, _, (_, _)>(8usize)
            )),
            |(a,b,c,d)| [a,b,c,d]
        )
    )(input)
}

fn payload(input: &[u8], length: usize) -> IResult<&[u8], &[u8]> {
    bytes::streaming::take(length)(input)
}

fn parse_websocket_frame_impl(input: &[u8]) -> IResult<&[u8], websocket::WebSocketFrame> {
    let (rem, ((fin, opc), (is_mask, payload_len))) =
        tuple((fin_and_opcode, is_mask_and_payload_length))(input)?;
    let (rem, final_payload_len) = match payload_len {
        0..=125 => (rem, payload_len),
        126 => extended_payload_length16(rem)?,
        127 => extended_payload_length64(rem)?,
        _ => return Err(nom::Err::Error((rem, nom::error::ErrorKind::TooLarge))),
    };
    let (rem, mk) = masking_key(rem)?;
    let (rem, payload) = payload(rem, final_payload_len as usize)?;

    Ok((
        rem,
        websocket::WebSocketFrame {
            fin,
            opcode: opc,
            is_masked: is_mask,
            payload_len: final_payload_len,
            payload: Vec::from_iter(payload.iter().map(|i| *i)),
            mask: mk,
        },
    ))
}

pub fn parse_websocket_frame(
    input: &[u8],
) -> Result<(websocket::WebSocketFrame, usize), error::RecoverableError> {
    match parse_websocket_frame_impl(input) {
        Ok((reminder, wsf)) => Ok((wsf, input.len() - reminder.len())),
        Err(e) => Err(e.into()),
    }
}
