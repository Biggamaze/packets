use nom::{
    bytes::streaming::{take, take_until},
    combinator::peek,
    IResult,
};
use std::io::{Error, ErrorKind, Read, Result, Write};

const CAPACITY: usize = 2048;

pub enum Frame {
    Complete(Vec<u8>),
    Incomplete(Vec<u8>),
}

/*
    HTTP używa /r/n/r/n do oznaczenia końca headerów
*/
pub fn read_frames<S: Read>(
    stream: &mut S,
    prefilled_buffer: Option<&[u8]>,
    delimiter: &[u8],
) -> Result<Vec<Frame>> {
    let mut buffer = [0u8; CAPACITY];
    let mut frames: Vec<Frame> = Vec::new();

    let mut previous_bytes = 0usize;

    if let Some(pb) = prefilled_buffer {
        previous_bytes = pb.len();
        if buffer.len() < previous_bytes {
            panic!("Buffer too short!");
        } else {
            buffer[..previous_bytes].copy_from_slice(pb);
        }
    }

    loop {
        match stream.read(&mut buffer[previous_bytes..]) {
            Err(e) => return Err(e),
            Ok(0) => return Err(Error::from(ErrorKind::ConnectionAborted)),
            Ok(bytes_read) => {
                let (rem, read_frames) =
                    read_all_delimited(&buffer[..previous_bytes + bytes_read], delimiter);
                let any_read = !read_frames.is_empty();

                for fr in read_frames {
                    frames.push(Frame::Complete(fr));
                }
                if let Some(fr_rem) = rem {
                    frames.push(Frame::Incomplete(fr_rem));
                }

                if any_read {
                    break;
                }
            }
        };
    }

    Ok(frames)
}

pub fn write<S: Write>(stream: &mut S, buffer: &[u8]) -> Result<usize> {
    stream.write(buffer)
}

fn read_all_delimited<'b>(buffer: &'b [u8], delimiter: &[u8]) -> (Option<Vec<u8>>, Vec<Vec<u8>>) {
    let mut vs: Vec<Vec<u8>> = Vec::new();
    let mut remaining_bytes = None;

    let mut parsing = buffer;

    loop {
        let mut v: Vec<u8> = Vec::new();
        match read_one_delimited(parsing, delimiter) {
            Ok((rem, chunk)) => {
                for byte in chunk {
                    v.push(*byte);
                }
                vs.push(v);
                parsing = rem;
            }
            Err(nom::Err::Incomplete(_)) => {
                if parsing.is_empty() {
                    break;
                }
                for byte in parsing {
                    v.push(*byte);
                }
                remaining_bytes = Some(v);
                break;
            }
            Err(e) => panic!("{}", e),
        }
    }

    (remaining_bytes, vs)
}

fn read_one_delimited<'b>(buffer: &'b [u8], delimiter: &[u8]) -> IResult<&'b [u8], &'b [u8]> {
    match peek(take_until(delimiter))(buffer) {
        Ok((input, matched)) => take(matched.len() + delimiter.len())(input),
        Err(error) => Err(error),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_one_delimited() {
        let buf = b"111\r\n\r\n222";

        let res = read_one_delimited(buf, b"\r\n\r\n");

        assert_eq!(Ok((&b"222"[..], &b"111\r\n\r\n"[..])), res);
    }

    #[test]
    fn test_read_all_delimited() {
        let buf = b"111\r\n\r\n222\r\n\r\n333";

        let res = read_all_delimited(buf, b"\r\n\r\n");

        let rem = String::from("333").into_bytes();
        let m1 = String::from("111\r\n\r\n").into_bytes();
        let m2 = String::from("222\r\n\r\n").into_bytes();

        assert_eq!((Some(rem), vec![m1, m2]), res);
    }
}
