use nom;
use std::{convert, fmt, io};

#[derive(Debug)]
pub enum RecoverableError {
    IoError(io::Error),
    BinaryParsingError(nom::Err<(Vec<u8>, nom::error::ErrorKind)>),
}

impl convert::From<io::Error> for RecoverableError {
    fn from(io_error: io::Error) -> Self {
        RecoverableError::IoError(io_error)
    }
}

impl convert::From<nom::Err<(&[u8], nom::error::ErrorKind)>> for RecoverableError {
    fn from(nom_error: nom::Err<(&[u8], nom::error::ErrorKind)>) -> Self {
        RecoverableError::BinaryParsingError(nom_error.to_owned())
    }
}

impl fmt::Display for RecoverableError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RecoverableError::IoError(ioe) => write!(f, "{}", ioe),
            RecoverableError::BinaryParsingError(bpe) => write!(f, "{}", bpe),
        }
    }
}
