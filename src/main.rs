mod error;
mod io;
mod parsing;
mod protocol;
mod sockets;

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate nom_trace;

use crate::{
    io::buffer,
    parsing::{http, websocket},
    protocol::http as phttp,
    protocol::websocket as pws
};
use std::net::{TcpListener, TcpStream};
use std::thread;

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8080")?;

    println!("TCP listener bound to : {}", listener.local_addr()?);

    loop {
        match listener.accept() {
            Ok((stream, address)) => {
                println!("New client : {}", address);
                thread::spawn(move || {
                    if let Err(e) = handle_client(stream) {
                        println!("Error : {}", e);
                    }
                });
            }
            Err(e) => println!("Couldn't accept client : {}", e),
        }
    }
}

fn handle_client(mut stream: TcpStream) -> std::io::Result<()> {
    let frames = sockets::read_frames(&mut stream, None, b"\r\n\r\n")?;

    // handle upgrade to WebSockets

    for frame in frames {
        match frame {
            sockets::Frame::Complete(bytes) => {
                let text = http::http_request(&bytes);
                match text {
                    Ok(request_http) => {
                        if phttp::is_websocket_upgrade_request(&request_http) {
                            println!("Received WebSocket upgrade request");
                            let response = phttp::prepare_websocket_upgrade_response(&request_http);
                            println!("Response\r\n{}", response);
                            let written =
                                sockets::write(&mut stream, response.to_string().as_bytes());
                            match written {
                                Ok(num) => println!("{} bytes written", num),
                                Err(e) => println!("{}", e),
                            }
                        }
                    }
                    Err(e) => panic!("{}", e),
                }
            }
            sockets::Frame::Incomplete(bytes) => {
                println!("Incomplete frame! : {:?}", bytes);
            }
        }
    }

    // work with WebSockets

    let mut reader = buffer::BufferedReader::new(stream.try_clone()?);

    loop {
        let mut frame = reader
            .consume_until_ok(&websocket::parse_websocket_frame)
            .expect("Should be ok!");
        frame.decode_payload();

        let data = std::str::from_utf8(&frame.payload).unwrap();
        println!("{:?} -> payload : {:?}", frame, data);

        let echo = pws::WebSocketFrame::text_response(data);

        println!("Echo frame {:?}", echo);

        let bytes : Vec<u8> = echo.into();

        let written 
            = sockets::write(&mut stream, &bytes);

        match written {
            Ok(num) => println!("{} bytes written", num),
            Err(e) => println!("{}", e),
        }
    }
}
