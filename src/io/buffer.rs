use crate::error;
use std::{io, io::Read};

const CAPACITY: usize = 4096;

pub struct BufferedReader<R> {
    reader: R,
    buffer: Vec<u8>,
    reusable_buffer: [u8; CAPACITY],
}

impl<R: Read> BufferedReader<R> {
    pub fn new(reader: R) -> Self {
        BufferedReader {
            reader,
            buffer: Vec::new(),
            reusable_buffer: [0u8; CAPACITY],
        }
    }

    fn read(&mut self) -> io::Result<usize> {
        let bytes_read = self.reader.read(&mut self.reusable_buffer)?;
        for byte in self.reusable_buffer[..bytes_read].iter() {
            self.buffer.push(*byte);
        }
        Ok(bytes_read)
    }

    pub fn consume<F, T, E>(&mut self, consumer: &F) -> Result<T, error::RecoverableError>
    where
        F: Fn(&[u8]) -> Result<(T, usize), E>,
        E: Into<error::RecoverableError>,
    {
        let _ = self.read()?;
        match consumer(&self.buffer) {
            Ok((result, read)) => {
                self.advance(read);
                Ok(result)
            }
            Err(e) => Err(e.into()),
        }
    }

    pub fn consume_until_ok<F, T, E>(&mut self, consumer: &F) -> Result<T, error::RecoverableError>
    where
        F: Fn(&[u8]) -> Result<(T, usize), E>,
        E: Into<error::RecoverableError>,
    {
        loop {
            match self.consume(consumer) {
                r @ Ok(_) => break r,
                _ => continue,
            }
        }
    }

    fn advance(&mut self, consumed: usize) {
        for _ in 0..consumed {
            self.buffer.remove(0);
        }
    }
}
